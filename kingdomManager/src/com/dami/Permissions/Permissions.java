package com.dami.Permissions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;

import com.dami.KingdomInfo.Kingdom;
import com.dami.KingdomInfo.KingdomMember;
import com.dami.KingdomInfo.KingdomRole;
import com.dami.kingdomManager.KingdomManager;

public class Permissions implements Listener {

	    public HashMap<UUID, PermissionAttachment> playerPermissions = new HashMap<>();
	    private ArrayList<Kingdom> kingdoms;
	    private KingdomManager kManager;
	    private Permissions perms = this;
	    public void onDisable() {
	        playerPermissions.clear();
	    }

	    public Permissions(KingdomManager _kingdomManager) {
	    	this.kManager = _kingdomManager;
	    	this.kingdoms = _kingdomManager.getKingdoms();
		}
	    
	    @EventHandler
	    public void join(PlayerJoinEvent event) {
	        Player player = event.getPlayer();
	        player.setGameMode(GameMode.SURVIVAL);
	        setupPermissions(player);
	    }
	    
	    @EventHandler
	    public void leave(PlayerQuitEvent event) {
	        Player player = event.getPlayer();
	        playerPermissions.remove(player.getUniqueId());
	    }
	    
	    public void setupPermissions(Player player) {
	        PermissionAttachment attachment = player.addAttachment(kManager);
	        this.playerPermissions.put(player.getUniqueId(), attachment);
	        if(getRole(player.getUniqueId()) != null) {
	        	permissionsSetter(player.getUniqueId(), getRole(player.getUniqueId()));
	        }
	        locationPerms(player.getUniqueId());
	    }
	    
	    private void permissionsSetter(UUID uuid, KingdomRole role) {
	        PermissionAttachment attachment = this.playerPermissions.get(uuid);
	        for (String type : kManager.getConfig().getConfigurationSection("kingdom").getKeys(false)) {
	        	if(type == role.toString().toLowerCase()) {
		            for (String tab : kManager.getConfig().getConfigurationSection("kingdom." + type).getKeys(false)) {
		            	for(String permissions : kManager.getConfig().getStringList("kingdom." + type + "." + tab)) {
			                System.out.print(permissions);
			                attachment.setPermission(permissions, true);
		            	}
		            }
	        	}
	        }
	    }
	    
	    private void locationPerms(UUID uuid) {
	    	PermissionAttachment attachment = this.playerPermissions.get(uuid);
	    	for(String permissions : kManager.getConfig().getStringList("kingdom.general.world")) {
	    		System.out.print(permissions);
	    		attachment.setPermission(permissions, true);
	    	}
	    	
	    }
	    
	    
	    public KingdomRole getRole(UUID id) {
	    	for(Kingdom kingdom : kingdoms) {
	    		for(KingdomMember member : kingdom.getMembers()) {
	    			if(id == member.getId()) {
	    				return member.getRole();
	    			}
	    		}
	    	}
	    	return null;
	    }
}

