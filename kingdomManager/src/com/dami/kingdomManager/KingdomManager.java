package com.dami.kingdomManager;

import java.util.ArrayList;

import org.bukkit.plugin.java.JavaPlugin;

import com.dami.KingdomInfo.Kingdom;
import com.dami.Permissions.Permissions;

public class KingdomManager extends JavaPlugin{
	
	private ArrayList<Kingdom> kingdoms = new ArrayList<Kingdom>();
	
	private Permissions permissions = new Permissions(this);
	
	private static KingdomManager instance;
	 public KingdomManager() 
	 {
		 instance = this;
	 }
	 
    public void onEnable() {
    	this.getConfig().options().copyDefaults(true);
    	this.saveConfig();
    	
    	getServer().getPluginManager().registerEvents(permissions, this);
    }
    
	 
	 public static KingdomManager getInstance() {
	        return instance;
	    }
	 
	 public Permissions getPermissions() {
		 return permissions;
	 }
	 
	 public ArrayList<Kingdom> getKingdoms(){
		 return kingdoms;
	 }
	 
	 public void setKingdoms(Kingdom kingdom) {
		 kingdoms.add(kingdom);
	 }

}
