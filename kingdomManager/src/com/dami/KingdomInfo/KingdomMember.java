package com.dami.KingdomInfo;

import java.util.UUID;

public class KingdomMember {
	private UUID id;
	private String customName;
    private long playTime;
    private long memberSince = System.currentTimeMillis();
    
    private KingdomClass job;
    private KingdomTag tag;
    private UUID kingdomID;
    private KingdomRole role;
    
    private boolean isActive = false;
    
    public KingdomMember(UUID _KDID, String _name,UUID _ID, KingdomRole _role) {
        this.setKingdomID(_KDID);
        this.setCustomName(_name);
        this.setRole(_role);
    }

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	public KingdomClass getJob() {
		return job;
	}

	public void setJob(KingdomClass job) {
		this.job = job;
	}

	public UUID getKingdomID() {
		return kingdomID;
	}

	public void setKingdomID(UUID kingdomID) {
		this.kingdomID = kingdomID;
	}

	public KingdomTag getTag() {
		return tag;
	}

	public void setTag(KingdomTag tag) {
		this.tag = tag;
	}

	public KingdomRole getRole() {
		return role;
	}

	public void setRole(KingdomRole role) {
		this.role = role;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public long getMemberSince() {
		return memberSince;
	}

	public void setMemberSince(long memberSince) {
		this.memberSince = memberSince;
	}

	public long getPlayTime() {
		return playTime;
	}

	public void setPlayTime(long playTime) {
		this.playTime = playTime;
	}
}
