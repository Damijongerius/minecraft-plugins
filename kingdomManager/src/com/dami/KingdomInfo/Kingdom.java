package com.dami.KingdomInfo;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Kingdom {
	@SuppressWarnings("unused")
	private final UUID ID;
    private String kingdom;
    
    private KingdomRank rank;

    private ArrayList<UUID> bannedPlayers = new ArrayList<>();
    private ArrayList<KingdomMember> members = new ArrayList<>();
    
    private ArrayList<Location> homes = new ArrayList<>();
    
    public Kingdom(UUID _ID,String _kingdom,Player _p) {
		this.ID = _ID;
		this.kingdom = _kingdom;
		generateProfile(_p, _ID, KingdomRole.KING);
		rank = KingdomRank.PRESETTLEMENT;
    	
    }
    
    public void generateProfile(Player _p, UUID _KingdomUUID, KingdomRole role) {
    	KingdomMember km = new KingdomMember(_KingdomUUID, _p.getDisplayName(), _p.getUniqueId(), role);
    	_p.sendMessage("welcome to:" + kingdom);
    	members.add(km);
    }
    
    public ArrayList<Location> getHomes(){
    	return homes;
    }
    
    public void setHomes(Location home) {
    	
    }
    
    public KingdomRank getRank() {
    	return rank;
    }

	public ArrayList<UUID> getBannedPlayers() {
		return bannedPlayers;
	}

	public void setBannedPlayers(ArrayList<UUID> bannedPlayers) {
		this.bannedPlayers = bannedPlayers;
	}
	
	public ArrayList<KingdomMember> getMembers() {
		return members;
	}
	
	public String getName() {
		return kingdom;
	}
}
