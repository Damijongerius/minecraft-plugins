package com.dami.main;

import java.util.ArrayList;

import org.bukkit.plugin.java.JavaPlugin;

import com.dami.KingdomClass.Territory;
import com.dami.claiming.ClaimSelectedArea;
import com.dami.selecting.CancelEvents;
import com.dami.selecting.Select;


public class KingdomClaims extends JavaPlugin {
	private Select select = new Select(this);
	private ClaimSelectedArea claimSelectedArea = new ClaimSelectedArea(this);
	private static KingdomClaims instance;
	
	private ArrayList<Territory> territories = new ArrayList<>();
	 
	 public KingdomClaims() 
	 {
		 instance = this;
	        
		 this.select = new Select(this);
		 this.claimSelectedArea = new ClaimSelectedArea(this);
	 }
	 
	@Override
	public void onEnable()
	{
		getServer().getPluginManager().registerEvents(new CancelEvents(this), this);
		getServer().getPluginManager().registerEvents(select, this);
	}
	
    
    public static KingdomClaims getInstance() {
        return instance;
    }
    
    public Select getSelect() {
    	return select;
    }
    
    public ClaimSelectedArea getClaimSA() {
    	return claimSelectedArea;
    }

	public ArrayList<Territory> getTerritories() {
		return territories;
	}

	public void addTerritories(Territory _territory) {
		territories.add(_territory);
	}
  
	

}
