package com.dami.KingdomClass;

import org.bukkit.block.Block;

public class CustomBlock{
	
	private int x;
	private int z;
	
	private int claimed;
	
	public CustomBlock(int _x,int _z) {
		this.x = _x;
		this.z = _z;
	}
	
	public int[] GetCords() {
		int[] xz = new int[] {x,z};
		return xz;
	}
	
	public int getClaimers() {
		return claimed;
	}
}
