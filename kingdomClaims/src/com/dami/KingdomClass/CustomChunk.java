package com.dami.KingdomClass;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.World;

public class CustomChunk {
	private int claimed;
	
	private int x;
	private int z;	
	
	private TinyChunk[][] TC = new TinyChunk[4][4];
	
	
	public CustomChunk(int _x, int _z) {
		this.x = _x;
		this.z = _z;
	}
	
	public boolean ReloadChunk(int x,int z,World world) {
		return false;
	}
	
	public int getKingdomID() {
		return claimed;
	}
	
	public int[] getCords() {
		int[] xz = new int[] {x,z}; 
		return xz;
	}
	
	public TinyChunk[][] getTinyChunks(){
		return TC;
	}
	
	public void addBlock(int[] tcIndex, int[] blockIndex, int x, int z) {
		if (TC[tcIndex[0]][tcIndex[1]] != null) {
			TC[tcIndex[0]][tcIndex[1]].AddBlock(blockIndex[0], blockIndex[1], x, z);
		}
		else {
			addTinyChunk(tcIndex[0],tcIndex[1]);
			TC[tcIndex[0]][tcIndex[1]].AddBlock(blockIndex[0], blockIndex[1], x, z);
		}
	}
	
	public void addTinyChunk(int indexX, int indexZ) {
		TC[indexX][indexZ] = new TinyChunk(UUID.randomUUID());
	}
	
	public ArrayList<Integer> GetPrecentages(){
		return null;
		
	}
	
	
}
