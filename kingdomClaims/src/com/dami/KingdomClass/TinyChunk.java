package com.dami.KingdomClass;

import java.util.UUID;

import org.bukkit.block.Block;

public 	class TinyChunk{
	private final UUID ID;
	private CustomBlock[][] blocks = new CustomBlock[4][4];
	private int claimed;
	
	public TinyChunk(UUID uuid) {
		this.ID = uuid;
	}
	
	public void AddBlock(int indexX, int indexZ, int x, int z) {
		blocks[indexX][indexZ] = new CustomBlock(x,z);
	}
}