package com.dami.KingdomClass;

import java.util.ArrayList;

public class Territory {
	
	private final int kingdomID;
	
	private ArrayList<CustomChunk> claimedChunks = new ArrayList<CustomChunk>();
    
    public Territory(int _kingdomID) {
        this.kingdomID = _kingdomID;
    }
    
    public int GetKingdomId() {
    	return kingdomID;
    }
    
    public ArrayList<CustomChunk> GetKingdomChunks() {
    	return claimedChunks;
    }
    public ArrayList<CustomChunk> UpdateKingdomChunks() {
    	return claimedChunks;
    }
}
