package com.dami.selecting;


import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.dami.main.KingdomClaims;
import com.dami.selecting.classes.PlayerData;
import com.dami.selecting.classes.Pos;



public class Select implements Listener{
	
	public ArrayList<PlayerData> players = new ArrayList<PlayerData>();
	
	@SuppressWarnings("unused")
	private KingdomClaims kingdomClaims;
	public Select(KingdomClaims _kingdomClaims) {
		this.kingdomClaims = _kingdomClaims;
	}


	private boolean comparePos(PlayerData _pd,Pos _pos) {
		for(Pos pos: _pd.matpos) {
			if(pos.x == _pos.x) {
				if(pos.y == _pos.y) {
					if(pos.z == _pos.z) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private int getMatIndex(PlayerData _pd,Pos _pos) {
		for(int i = 0; i < _pd.matpos.size(); i++) {
			Pos pos = _pd.matpos.get(i);
			if(pos.x == _pos.x) {
				if(pos.y == _pos.y) {
					if(pos.z == _pos.z) {
						return i;
					}
				}
			}
		}
		return -1;
	}
	
	private void cancelClaiming(PlayerData _pd) {
		for(int i = 0; i < _pd.matpos.size(); i++) {
			Block block = _pd.player.getWorld().getBlockAt(_pd.matpos.get(i).LocationConvert(_pd.player.getWorld()));
			block.setType(_pd.mats.get(i));
		}
	}
	
	@EventHandler
	public void PlayerSelect(PlayerInteractEvent e) {
		Material marking = Material.LIME_CONCRETE;
		Pos pos = new Pos();
		Player p = e.getPlayer();
		Block clicked = e.getClickedBlock();
		//p.sendMessage("size:" + players.size());
		
		for (int i = 0; i < players.size(); i++) {
			if(players.get(i).player == p) 
			{
				e.setCancelled(true);
				PlayerData pd = players.get(i);
				pos.Set(clicked.getX(), clicked.getY(), clicked.getZ());
				if(clicked.getType() != null && e.getAction() == Action.LEFT_CLICK_BLOCK) 
				{
					//p.sendMessage("x(" + clicked.getX() + ") y(" + clicked.getY() + ") z(" + clicked.getZ() + ")");
					
					if(comparePos(pd, pos)) 
					{
						if (p.getInventory().getItemInMainHand().getType() == Material.WOODEN_HOE) 
						{	
							pd.mats.add(clicked.getType());
							p.sendMessage("mats:" + pd.mats.size());
							pd.matpos.add(pos);
							clicked.setType(marking);
						}
						else return;
					}
					else return;
				}
				else if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
					if (p.getInventory().getItemInMainHand().getType() == Material.WOODEN_HOE) 
					{	
						try {
							clicked.setType(pd.mats.get(getMatIndex(pd, pos)));
							players.get(i).mats.remove(getMatIndex(pd, pos));
							players.get(i).matpos.remove(getMatIndex(pd, pos));
							p.sendMessage("mats:" + pd.mats.size());
						} catch (Exception e1) {
							
						}

					}
					else return;
				}
				else return;
			}
			else return;
		}
		
	}
	
    @EventHandler
    public void breakBlock(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (!player.hasPermission("world.blockbreak")) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You do not have permission to do that bro!");
        }
    }
	
    public void AddSelector(Player _p) {
		 ItemStack item = new ItemStack(Material.WOODEN_HOE, 1);
		 PlayerData pd = new PlayerData();
		 boolean isClaiming = false;
		 int index = 0;
		 
		 for(int i = 0; i < players.size(); i++) {
				if(_p.getName() == players.get(i).player.getName()) {
					_p.sendMessage("loop:" + i);
					isClaiming = true;
					index = i;
				}
			}
		 
		 if(isClaiming == false) {
			 for (int j=8;j>=0;j--) {
				 if(_p.getInventory().getItem(j) == null) {
					 _p.getInventory().addItem(item);
					 _p.getInventory().setHeldItemSlot(j);
					 
					 
					 _p.sendMessage(ChatColor.GOLD + "--------------\n" +ChatColor.GREEN +"You have activated claim mode\n"+ChatColor.WHITE + "do /k help for command list\n " + ChatColor.GOLD + "--------------");
					 pd.player = _p;
					 players.add(pd);
					 break;
				 }
				 else 
				 {
					 if(j == 0) {
						 _p.sendMessage(ChatColor.DARK_RED +"It was not possible to execute this command as you don't have an empty inventory slot in your hotbar");
					 }
					 continue;
				 }
			 }
		 }
		 else {
			 for (int j=8;j>=0;j--) 
			 {
				 if (_p.getInventory().getItem(j) != null) {
					if (_p.getInventory().getItem(j).getType() == item.getType()) {
						_p.getInventory().getItem(j).setAmount(0);
						cancelClaiming(players.get(index));
						_p.sendMessage(ChatColor.GOLD + "--------------\n" + ChatColor.RED
								+ "You have deactivated claim mode\n" + ChatColor.GOLD + "--------------");
						players.remove(players.get(index));
						break;
					} 
				}
			 }
		 }
    }
    
    public ArrayList<PlayerData> getPlayers(){
    	return players;
    }
}
