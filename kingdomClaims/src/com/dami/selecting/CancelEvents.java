package com.dami.selecting;

import java.util.ArrayList;

import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.dami.main.KingdomClaims;
import com.dami.selecting.classes.PlayerData;

public class CancelEvents implements Listener {
	
	private KingdomClaims kingdomClaims;
	private Select select;
	public CancelEvents(KingdomClaims _kingdomClaims) {
		this.kingdomClaims = _kingdomClaims;
		this.select = kingdomClaims.getSelect();
	}
	
	public ArrayList<PlayerData> players = new ArrayList<PlayerData>();

	@EventHandler
	public void ICV(InventoryClickEvent e) {
		HumanEntity p = e.getWhoClicked();
		reloadPlayers();
		for(int i = 0; i < players.size(); i++) {
			if(p.getName() == players.get(i).player.getName()) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void PDIE(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		reloadPlayers();
		for(int i = 0; i < players.size(); i++) {
			if(p.getName() == players.get(i).player.getName()) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void PHI(EntityDamageByEntityEvent e) {
		Entity attacker = e.getDamager();
		Entity taker = e.getEntity();
		reloadPlayers();
			for(int i = 0; i < players.size(); i++) {
				if(taker.getName() == players.get(i).player.getName()) {
					if(attacker instanceof Mob) {
						((Mob) attacker).setHealth(0);
					}
					e.setCancelled(true);
				}
		}
	}
	
	private void reloadPlayers() {
		if (select != null) {
			if (select.getPlayers() != null) {
				players = select.getPlayers();
			}
		}
	}
}
