package com.dami.selecting.classes;


import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.World;

public class Pos {
	public int x;
	public int y;
	public int z;
	
	public ArrayList<Dep> deps = new ArrayList<>();
	
	public int diff;
	
	public Pos cordAbove;
	
	public boolean hasAbove = false;
	public boolean cancel = false;
	
	public void Set(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Location LocationConvert(World world) {
		Location location = new Location(world, x, y, z);
		return location;
	}
	
	public class Dep{
		public int depX; 
		public int depY; 
		public int depZ; 
		
		public Dep(Pos dep) {
			depX = dep.x;
			depY = dep.y;
			depZ = dep.z;
		}
	}
	
	public void reset() {
		deps = new ArrayList<>();
		
		diff = 0;
		
		cordAbove = null;
		
		hasAbove = false;
		cancel = false;
	}
	
	public void addDep(Pos newDep)
	{
		Dep dep = new Dep(newDep);
		deps.add(dep);
	}
}
