package com.dami.claiming;

import java.util.ArrayList;


import org.bukkit.Location;
import org.bukkit.Particle;

import com.dami.selecting.classes.PlayerData;
import com.dami.selecting.classes.Pos;
import com.dami.selecting.classes.Pos.Dep;

public class Circle {

	ArrayList<Pos> positions = new ArrayList<Pos>();
	
	ArrayList<Pos> fullPositions = new ArrayList<Pos>();
	
	public boolean IsPossible(PlayerData _pd) {
		_pd.player.sendMessage("creating claim");
		this.positions = _pd.matpos;
		for(int i = 0; i < positions.size(); i++) {
			
			positions.get(i).reset();
			
		}
		//calculating blocks slected next to this i
		for(int i = 0; i < positions.size(); i++) {
			
			for(int j = 0; j < positions.size(); j++) {
				if(positions.get(i).cancel == false) {
					if (positions.get(i).x + 1 == positions.get(j).x
							|| positions.get(i).x - 1 == positions.get(j).x
							|| positions.get(i).x == positions.get(j).x) {
						
						if (positions.get(i).z + 1 == positions.get(j).z
								|| positions.get(i).z - 1 == positions.get(j).z
								|| positions.get(i).z == positions.get(j).z) {
							
							positions.get(i).addDep(positions.get(j));
							continue;
						}
					}
				}
			}
		}
		
		//calculating the nearest on axis
		for(int i = 0; i < positions.size(); i++) {
			if(positions.get(i).cancel == false) {
				
				//not working
				if(isNextInRange(i,_pd)) {
					positions.get(i).cordAbove = getHigherOnAxis(i,true, true);
				}
				positions.get(i).cancel = true;
			}
			else 
			{
				positions.get(i).diff = 1;
			}
		}
		for(int i = 0; i < positions.size(); i++) {
			if(positions.get(i).deps.size() >= 3) {
				continue;
			}
			else {
				_pd.player.sendMessage("sorry cant have less then 2blocks next to eachother{" + positions.get(i).deps.size() + "," + positions.get(i).x + "," + positions.get(i).z + "}");
				return false;
			}
		}
		drawMap(_pd);
		return true;
	}
	
	//getting the next location on axis
	public Pos getHigherOnAxis(int i, boolean axis, boolean minusPlus)
	{
		Pos pos = null;
		int first;
		int second;
		int third;
		//false? 
			for(int j = 0; j < positions.size(); j++) {
				if(axis) {
					//use x
					first = positions.get(i).x;
					second = positions.get(j).x;
					third = positions.get(i).z - positions.get(j).z;
					
				}else {
					//use y
					first = positions.get(i).z;
					second = positions.get(j).z;
					third = positions.get(i).x - positions.get(j).x;
				}
				if(minusPlus) {
					if(first < second && third == 0) {
						pos = positions.get(j);
						return pos;
					}
				}else {
					if(first > second && third == 0) {
						pos = positions.get(j);
						return pos;
					}
				}
			
		}
		return pos;
	}
	
	//checking if the next one is in range
	public boolean isNextInRange(int i, PlayerData _pd) {
		Pos pos = getHigherOnAxis(i, true, true);
		if(pos != null) {
			Pos posZ = null;
			for(Dep dep : positions.get(i).deps) {
				if(dep.depZ == positions.get(i).z -1) {
					posZ = getHigherOnAxis(i, false, true);
				}
				if(dep.depZ == positions.get(i).z +1) {
					posZ = getHigherOnAxis(i, false, false);
				}
				
				if(posZ != null) {
					return true;
				}
			}
			return true;
		}
		else return false;
	}	
	
	public void drawMap(PlayerData _pd) {
		for(int i = 0; i < positions.size(); i++) {
			int diff = 0;
			if(positions.get(i).cordAbove != null) {
				if(positions.get(i).cordAbove.x < 0 && positions.get(i).x > 0) {
					
					diff = ((positions.get(i).cordAbove.x - positions.get(i).x)*-1);
					//_pd.player.sendMessage("diff" + positions.get(i).cordAbove.x + "," + positions.get(i).cordAbove.z + " | " + positions.get(i).x + "," + positions.get(i).z);
				}else {
					diff = positions.get(i).cordAbove.x - positions.get(i).x;
					//_pd.player.sendMessage("diff" + positions.get(i).cordAbove.x + "," + positions.get(i).cordAbove.z + " | " + positions.get(i).x + "," + positions.get(i).z);
				}
			}else {
				diff = 1;
			}
			for(int j = 0; j < diff; j++) {
				Pos pos = new Pos();
				pos.x =positions.get(i).x + j;
				pos.z = positions.get(i).z;
				fullPositions.add(pos);
				Location loca = new Location(_pd.player.getWorld(),(positions.get(i).x + j + 0.5), _pd.player.getLocation().getY() + 1, (positions.get(i).z + 0.5));
				_pd.player.getWorld().spawnParticle(Particle.COMPOSTER,loca , 10);
			}
		}
	}
}


//1,-1| 1,0 |1,1
//----+-----+----
//0,-1| 0,0 |0,1
//----+-----+----
//-1-1|-1,0 |-1,1

