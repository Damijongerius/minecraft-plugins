package com.dami.claiming;

import org.bukkit.entity.Player;

import com.dami.main.KingdomClaims;
import com.dami.selecting.Select;
import com.dami.selecting.classes.PlayerData;

public class ClaimSelectedArea {
	Circle circle = new Circle();
	private KingdomClaims kingdomClaims;
	public ClaimSelectedArea(KingdomClaims _kingdomClaims) {
		this.kingdomClaims = _kingdomClaims;
	}
	
	PlayerData pd = new PlayerData();

	public void Claim(Player p) {
		Select select = kingdomClaims.getSelect();
		for(int i = 0; i < select.players.size(); i++) {
			if(select.players.get(i).player == p) {
				pd = select.players.get(i);
				break;
			}
			else {
				pd = null;
			}
		}
		if(circle.IsPossible(pd)) {
			this.pd.matpos = circle.positions;
			
		}
		
	}
}
