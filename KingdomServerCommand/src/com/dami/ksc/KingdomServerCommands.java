package com.dami.ksc;

import org.bukkit.plugin.java.JavaPlugin;

import com.dami.Commands.KingdomCommand;
import com.dami.Commands.KingdomCommandComplete;
import com.dami.kingdomManager.KingdomManager;
import com.dami.main.KingdomClaims;


public class KingdomServerCommands extends JavaPlugin
{
	@Override
	public void onEnable()
	{
		KingdomClaims kingdomClaims = KingdomClaims.getInstance();
		KingdomManager kingdomManager = KingdomManager.getInstance();
		
		
		new KingdomCommand(this,kingdomClaims,kingdomManager);
		new KingdomCommandComplete(this);
	}
}

