package com.dami.Commands;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.fusesource.jansi.Ansi.Color;

import com.dami.KingdomInfo.Kingdom;
import com.dami.KingdomInfo.KingdomMember;
import com.dami.claiming.ClaimSelectedArea;
import com.dami.kingdomManager.KingdomManager;
import com.dami.ksc.KingdomServerCommands;
import com.dami.main.KingdomClaims;
import com.dami.selecting.Select;

public class KingdomCommand implements CommandExecutor {
	
	private KingdomServerCommands plugin;
	private Select select;
	private ClaimSelectedArea claimSA;
	private KingdomManager kingdomManager;
	public KingdomCommand(KingdomServerCommands _main, KingdomClaims _kingdomClaims, KingdomManager _kingdomManager) {
		this.plugin = _main;
		this.select = _kingdomClaims.getSelect();
		this.claimSA = _kingdomClaims.getClaimSA();
		this.kingdomManager = _kingdomManager;
		plugin.getCommand("k").setExecutor(this);
		plugin.getCommand("km").setExecutor(this);
	}


	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(!(sender instanceof Player)) 
		{
			sender.sendMessage("sender must be a player");
			return true;
		}
		
		Player p = (Player) sender;
		
		if(args.length <= 0) 
		{
			p.sendMessage(ChatColor.DARK_RED +"This is not a command do /help");
		}
		else 
		{
			String input0 = args[0];
			switch(cmd.getName()) 
			{
				case "k":
				{
				
					switch(input0) {
					case "help":
					{
						if (args.length == 1) {
							p.sendMessage(ChatColor.GOLD + "--------------\n" + ChatColor.WHITE
									+ "Here is the list of Kingdom commands\n" + " -k help\n" + " -k spawn\n"
									+ " -k info\n" + " -k report <reason>\n" + "Here are the higherrank commands\n "
									+ " -km help" + " -km upgrade\n" + " -km ideology\n" + " -km story\n"
									+ " -km claim\n" + "   -km claim create\n" + "   -km claim delete\n"
									+ "   -km claim info\n" + ChatColor.GOLD + "--------------");
						}
					}
				    default:
					p.sendMessage(ChatColor.DARK_RED +"This is not a command (only help)");
					break;
				}
				break;
			}
			case "km":
			{
				switch(input0) 
				{
					case "claim":
					{
						if(sender.hasPermission("claim.claim")) {
							
						
						if(args.length == 2) {
							String input1 = args[1];
							switch(input1) 
							{
								case "select":
								{
									select.AddSelector(p);
									break;
								}
								case "create":
								{
									claimSA.Claim(p);
									break;
								}
								case "delete":
								case "info":
								break;
								default:
									p.sendMessage(ChatColor.DARK_RED +"This is not a argument");
									break;
								}	
							}
							else 
							{
								p.sendMessage(ChatColor.DARK_RED +"a second argument is needed for this command select, create, delete or info");
							}
						}
						else 
						{
							p.sendMessage(ChatColor.DARK_RED +"you don't have the permissions to use this command");
						}
						break;
					}
					case "chunk":{
						Chunk chunk = p.getLocation().getChunk();
						p.sendMessage("chunk:" + chunk);
						break;
					}
					case "create":
					{
						if(args.length == 2) 
						{
							String input1 = args[1];
							if(!(hasKingdom(p.getUniqueId()))) {
								UUID uuid = UUID.randomUUID();
								Kingdom kingdom = new Kingdom(uuid, input1, p);
								kingdomManager.setKingdoms(kingdom);
								p.sendMessage("You created your kingdom read the tasks you need to do to maintain a kingdom name:" + input1);
								break;
							}
							else {
								p.sendMessage(Color.RED + "You are already in a kingdom leave before trying to make your own");
							}
							
						}
					}
					default:
						p.sendMessage(ChatColor.DARK_RED +"This is not a command (as a)");
						break;
				}
			}
		}		    
	}
	return false;
	

	}
	private boolean hasKingdom(UUID id) {
		for(Kingdom kingdom: kingdomManager.getKingdoms()) {
			for(KingdomMember member : kingdom.getMembers()) {
				if(id == member.getId()) {
					return true;
				}
			}
		}
		return false;
		
	}
}