package com.dami.Commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import com.dami.kingdomManager.KingdomManager;
import com.dami.ksc.KingdomServerCommands;

public class KingdomCommandComplete implements TabCompleter {

	private KingdomServerCommands plugin;
	private KingdomManager kingdomManager;
	public KingdomCommandComplete(KingdomServerCommands _main) {
		this.plugin = _main;
		plugin.getCommand("k").setTabCompleter(this);
		plugin.getCommand("km").setTabCompleter(this);
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		List<String> options = new ArrayList<String>();
		
		switch(cmd.getName()) {
		
		case "k":{
			
			if(args.length == 1) {
				options.add("help");
				
			}
			break;
			
		}
		case "km":{
			if(args.length == 1) {
				options.add("claim");
				options.add("chunk");
				options.add("create");
				
			}
			if(args.length == 2) {
				switch(args[0]) {
				
				case "claim":{
					options.add("select");
					options.add("create");
					options.add("delete");
					break;
				}
				case "create":{
					options.add("<kingdomName>");
					break;
				}
				
				default:{
					return null;
				}
				}
			}
			break;
		}
		default:{
			return null;
		}
		}
		if(options.size() == 0) {
			return null;
		}
		return options;
	}

}
